package nz.co.vectorams.spike.api;

import java.util.Map;
import java.util.Objects;

public class ApiGatewayRequestContext {

    private String accountId;
    private String resourceId;
    private String stage;
    private String requestId;
    private Map<String, String> identity;
    private String cognitoIdentityPoolId;
    private String resourcePath;
    private String httpMethod;
    private String apiId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Map<String, String> getIdentity() {
        return identity;
    }

    public void setIdentity(Map<String, String> identity) {
        this.identity = identity;
    }

    public String getCognitoIdentityPoolId() {
        return cognitoIdentityPoolId;
    }

    public void setCognitoIdentityPoolId(String cognitoIdentityPoolId) {
        this.cognitoIdentityPoolId = cognitoIdentityPoolId;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiGatewayRequestContext that = (ApiGatewayRequestContext) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(resourceId, that.resourceId) &&
                Objects.equals(stage, that.stage) &&
                Objects.equals(requestId, that.requestId) &&
                Objects.equals(identity, that.identity) &&
                Objects.equals(cognitoIdentityPoolId, that.cognitoIdentityPoolId) &&
                Objects.equals(resourcePath, that.resourcePath) &&
                Objects.equals(httpMethod, that.httpMethod) &&
                Objects.equals(apiId, that.apiId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, resourceId, stage, requestId, identity, cognitoIdentityPoolId, resourcePath, httpMethod, apiId);
    }

    @Override
    public String toString() {
        return "ApiGatewayRequestContext{" +
                "accountId='" + accountId + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", stage='" + stage + '\'' +
                ", requestId='" + requestId + '\'' +
                ", identity='" + identity + '\'' +
                ", cognitoIdentityPoolId=" + cognitoIdentityPoolId +
                ", resourcePath='" + resourcePath + '\'' +
                ", httpMethod='" + httpMethod + '\'' +
                ", apiId='" + apiId + '\'' +
                '}';
    }
}
