package nz.co.vectorams.spike.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.Set;
import java.util.zip.GZIPInputStream;

public class ApiGatewayRequestHandler implements RequestHandler<ApiGatewayRequest, ApiGatewayResponse> {
    private static final Set<String> COMPRESSED_PAYLOAD_TYPES = Collections.singleton("application/vnd.json+gzip");
    private static final String CONTENT_TYPE_REQUEST_HEADER = "Content-Type";

    public ApiGatewayResponse handleRequest(final ApiGatewayRequest request, final Context context) {
        final ApiGatewayResponse response = new ApiGatewayResponse();
        final LambdaLogger logger = context.getLogger();
        try {

            logger.log("Request:\n" + request);

            // Assume that the payload for the request is a string
            String payload;

            if (request.getIsBase64Encoded()) {
                logger.log("Request body is Base64 encoded");
                Base64.Decoder decoder = Base64.getDecoder();
                InputStream decodedBodyStream = decoder.wrap(new ByteArrayInputStream(request.getBody().getBytes(StandardCharsets.UTF_8)));

                if (!request.getHeaders().containsKey(CONTENT_TYPE_REQUEST_HEADER)) {
                    throw new Exception("Content type request header missing");
                }
                String requestContentType = request.getHeaders().get(CONTENT_TYPE_REQUEST_HEADER);
                InputStream payloadStream;
                if (COMPRESSED_PAYLOAD_TYPES.contains(requestContentType)) {
                    logger.log("Request body is GZIP compressed");
                    payloadStream = new GZIPInputStream(decodedBodyStream);
                } else {
                    payloadStream = decodedBodyStream;
                }

                payload = convert(payloadStream, StandardCharsets.UTF_8);
            } else {
                logger.log("Request body is a standard string");
                payload = request.getBody();
            }

            logger.log("Request Payload:\n" + payload);

            response.setBody(payload);
            response.setBase64Encoded(false);
            response.setStatusCode("200");
        } catch (Exception exception) {
            logger.log("Error: " + exception.getMessage());
            exception.printStackTrace();
            response.setBody(exception.getMessage());
            response.setStatusCode("500");
        }

        logger.log("Response: " + response);
        return response;
    }

    public String convert(InputStream inputStream, Charset charset) throws IOException {

        StringBuilder stringBuilder = new StringBuilder();
        String line = null;

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset))) {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }

        return stringBuilder.toString();
    }
}
