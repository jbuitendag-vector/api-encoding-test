package nz.co.vectorams.spike.api;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ApiGatewayResponse {

    private Boolean isBase64Encoded;
    private String statusCode;
    private Map<String, String> headers;
    private Map<String, List<String>> multiValueHeaders;
    private String body;

    public Boolean getBase64Encoded() {
        return isBase64Encoded;
    }

    public void setBase64Encoded(Boolean base64Encoded) {
        isBase64Encoded = base64Encoded;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, List<String>> getMultiValueHeaders() {
        return multiValueHeaders;
    }

    public void setMultiValueHeaders(Map<String, List<String>> multiValueHeaders) {
        this.multiValueHeaders = multiValueHeaders;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiGatewayResponse that = (ApiGatewayResponse) o;
        return Objects.equals(isBase64Encoded, that.isBase64Encoded) &&
                Objects.equals(statusCode, that.statusCode) &&
                Objects.equals(headers, that.headers) &&
                Objects.equals(multiValueHeaders, that.multiValueHeaders) &&
                Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isBase64Encoded, statusCode, headers, multiValueHeaders, body);
    }

    @Override
    public String toString() {
        return "ApiGatewayResponse{" +
                "isBase64Encoded=" + isBase64Encoded +
                ", statusCode='" + statusCode + '\'' +
                ", headers=" + headers +
                ", multiValueHeaders=" + multiValueHeaders +
                ", body='" + body + '\'' +
                '}';
    }
}
